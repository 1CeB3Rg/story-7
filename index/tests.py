from django.test import TestCase,Client
from django.urls import resolve
from .views import home,confirmed
from .models import Status
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
# Create your tests here.
class story7test(TestCase):
    def test_url_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_using_func(self):
        found=resolve('/')
        self.assertEqual(found.func,home)
    def test_home_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_using_func2(self):
        found=resolve('/confirm')
        self.assertEqual(found.func,confirmed)
    
    def test_model_exist(self):
        Status.objects.create(userName = "Bob Ross", userStatus = "Chillax")
        countcontent = Status.objects.all().count()
        self.assertEqual(countcontent, 1)
    #def test_functional(self):
    #    driver=webdriver.Firefox()
    #    driver.get('http://localhost:8000')
    #    time.sleep(1)
    #    name=driver.find_element_by_id("id_userName")
    #    status=driver.find_element_by_id("id_userStatus")
    #    name.send_keys("Putin")
    #    status.send_keys("Vodka")
    #    name.send_keys(Keys.RETURN)
    #    time.sleep(2)
        
    #    assert "Putin" in driver.page_source
    #    assert "Vodka" in driver.page_source
        # button=driver.find_element_by_id("Submitted")
        # button.click()
        # assert "Putin" in driver.page_source
        # assert "Vodka" in driver.page_source
    #   time.sleep(5)
    #    driver.quit()
    