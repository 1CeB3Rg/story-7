from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm
def home(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            inputName = form.data['userName']
            inputStatus = form.data['userStatus']
            request.session['inputName'] = inputName
            request.session['inputStatus'] = inputStatus
            # data_item = form.save(commit=False)         
            return redirect('/confirm')

    
    else:
        form= StatusForm()
        data= Status.objects.all()
        response={'Data':data,'status':form}
        return render(request,'home.html',response)
def confirmed(request):
    inputName = request.session['inputName']
    inputStatus = request.session['inputStatus']
    if request.method == 'POST':
        if "cancel" in request.POST:
            return redirect('/')
        else:

            Status.objects.create(userName = inputName, userStatus = inputStatus)
            return redirect('/')
    else:
        context = {
            'nameName': inputName,
            'statusStatus': inputStatus,
        }
        return render(request, 'confirm.html', context)

